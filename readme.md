# Hunter

### Tech stack

* React (Views)
* Redux (Data Flow)
* Webpack (Builder)
* Babel (es6/es2015)
* SASS and PostCSS (Styles)
* Karma (Test runner)
* Jasmine (Unit testing)

### Running it locally

You'll need npm and node, if you don't have so, please install it first.

Then clone the repo

`git clone https://aganglada@bitbucket.org/aganglada/hunter.git`

this will create a folder called hunter, go inside

`cd hunter`

and install dependencies (I will recommend you to take a coffee, npm is super fast :p)

`npm install`

then we can bundle our application 

`npm run dev`

this will bundle js/css and serve them for you.

Now enter `http://0.0.0.0:8080/` in your preferred browser and enjoy hunting!

### Running the tests

You have two options:

* `npm run test:watch`: Will watch your changes in the code and update the test results automatically.
* `npm run test:prod`: Will do single run test

### Browser support

Tested in the latest versions of Chrome and Firefox

### What I missed to do

* I would like to do more error catching and so write better tests for my services
* Not really happy with the design, not sexy enough for me.
* Show messages in more user friendly way (not using alert / don't kill me for it)


```
NOTE:

I've only tested important services and reducers functions as I try to keep logic out of the 
views and hundle every important transaction on them.
```



