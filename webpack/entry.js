'use strict';

module.exports = function (test) {
    var obj = {
        app: './app/app.js',
    };

    return test ? {} : obj;
};
