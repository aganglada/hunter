import sortByKey from 'services/sort-by-key/sort-by-key';

describe('sortByKey', () => {
    let array = [{
        name: 'medusa',
        age: 100,
        mana: 7008
    }, {
        name: 'hippogriffs',
        age: 200,
        mana: 6000
    }];

    it('should be a function', () => {
        expect(typeof sortByKey).toBe('function');
    });

    it('should return an array of objects', () => {
        expect(Array.isArray(sortByKey(array, 'name'))).toBe(true);
    });

    it('should return an array sorted array sorting by age', () => {
        expect(sortByKey(array, 'age')).toEqual(array);
    });

    it('should return an array sorted array sorting by mana', () => {
        expect(sortByKey(array, 'mana')).toEqual(array.reverse());
    });
});
