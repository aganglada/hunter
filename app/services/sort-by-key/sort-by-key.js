export default (array, key) => {
    return array.concat().sort((a ,b) => {
        let alc = a[key].toString().toLowerCase(), blc = b[key].toString().toLowerCase();
        return alc > blc ? 1 : alc < blc ? -1 : 0;
    });
};
