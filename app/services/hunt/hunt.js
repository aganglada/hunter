import random from 'services/random/random';

/**
 * Hunt service
 * It's the one goes to the whole flow of hunting.
 *
 * It works in the following way:
 *
 * First, it checks that in the store we don't have 10 creatures already.
 * Then it toggles the panel to start hunting (Looking for creatures).
 * Ones the panel is open in a random time between 1 and 15 seconds, it
 * decides if you found or not the creature (defining again a random number
 * that access it to found array with a 66% provability that you've found something)
 * in the case you've found a creature it flips the panel to allow you setting a new name.
 * Otherwise it will toggle and flip again the panel and send yuo a message.
 */

const found = [true, false, true];

const hunt = () => {
    const start = (store) => {
        const { creatures } = store.getState();

        if (creatures.length < 10) {

            store.dispatch({ type: 'TOGGLE' });

            setTimeout(() => {
                let n = random(0, 2);
                if (found[n]) {
                    store.dispatch({ type: 'FLIP' });
                } else {
                    store.dispatch({ type: 'TOGGLE' });
                    alert('HAHA! You missed!')
                }

            }, random(1000, 15000));
        } else {
            alert('You are not allow to hunt more since you have 10 creatures already');
        }
    };

    return {
        start
    };
};

export default hunt();
