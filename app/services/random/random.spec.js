import random from 'services/random/random';

describe('random', () => {
    let n;
    const min = 0;
    const max = 40;

    it('should be a function', () => {
        expect(typeof random).toBe('function');
    });

    beforeEach(() => {
        n = random(min, max);
    });

    it('should return an integer', () => {
        expect(Number(n) === n && n % 1 === 0).toBe(true);
    });

    it('should return integer between a given range', () => {
        expect(n >= min && n <= max).toBe(true);
    });
});
