import sumByKey from 'services/sum-by-key/sum-by-key';

describe('sumByKey', () => {
    let array = [{
        name: 'medusa',
        age: 100,
        mana: 7008
    }, {
        name: 'hippogriffs',
        age: 200,
        mana: 6000
    }];

    it('should be a function', () => {
        expect(typeof sumByKey).toBe('function');
    });

    it('should return an integer', () => {
        const n = sumByKey(array, 'age');
        expect(Number(n) === n && n % 1 === 0).toBe(true);
    });

    it('should return a true sum sorting by age', () => {
        expect(sumByKey(array, 'age')).toEqual(300);
    });

    it('should return a true sum sorting by mana', () => {
        expect(sumByKey(array, 'mana')).toEqual(13008);
    });
});
