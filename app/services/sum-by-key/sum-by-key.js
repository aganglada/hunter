export default (array, key) => {
    if (array.length === 0) {
        return 0;
    }

    if (array.length === 1) {
        return array[0][key];
    }

    return array.reduce((prev, next) => {
        return (typeof prev === 'object' ? prev[key] : prev) + next[key];
    });
};
