import url from 'services/url/url';

describe('sumByKey', () => {

    const object = {
        "center": "7-8+St+Martin's+Place,London,WC2N",
        "zoom": "13",
        "scale": "4",
        "size": "2000x2000",
        "maptype": "roadmap"
    };

    it('should be an object', () => {
        expect(typeof url).toBe('object');
    });

    it('should have a method called objectToParams', () => {
        expect(typeof url.objectToParams).toBe('function');
    });

    it('should return an string, nothing else', () => {
        expect(typeof url.objectToParams(object)).toBe('string');
    });
});
