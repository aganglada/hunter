const url = () => {
    const objectToParams = (obj) => {
        let uri = '';

        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                uri = `${uri}&${key}=${obj[key]}`;
            }
        }

        return uri;
    };

    return {
        objectToParams
    };
};

export default url();
