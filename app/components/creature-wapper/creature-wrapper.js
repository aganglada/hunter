const creatureWrapper = (props) => {

    const elementClassName = `col-xs-12 col-sm-6 col-md-3 u-text-align--center${props.hidden ? ' u-visually-hidden' : ''}`;

    return (
        <li className={elementClassName}>
            <div className="c-creature">
                <div className="c-creature__inner">
                    <div className="c-creature__wrapper">
                        <div className="c-creature__content">
                            { props.children }
                        </div>
                    </div>
                </div>
            </div>
        </li>
    );
};

export default creatureWrapper;
