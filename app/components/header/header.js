export default () => {
    return (
        <header className="c-header">
            <span className="u-text-align--center u-display--block">Hunter Manger</span>
        </header>
    );
}
