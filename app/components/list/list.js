import Creature from 'components/creature/creature';
import Filters from 'components/filters/filters';

const renderList = creatures => {
    return (<ul className="row">{creatures.map((creature, key) => {
        return <Creature creature={creature} key={key} />;
    })}</ul>);
};

const renderNoCreatures = () => <p className="u-text-align--center u-padding">You don't have creatures, go out there and hunt for more!</p>;

const list = ({
    creatures,
    totalAge,
    totalMana,
}) => {
    return (
        <section className="u-margin--zero-auto u-max-width--960">
            <Filters />
            <aside className="row u-text-align--center u-font-color--04-lighter">
                <div className="col-xs-12 col-md-4 u-padding u-box-shadow">Empty cages {10 - creatures.length}</div>
                <div className="col-xs-12 col-md-4 u-padding u-box-shadow">Total age {totalAge}</div>
                <div className="col-xs-12 col-md-4 u-padding u-box-shadow">Total mana {totalMana} </div>
            </aside>
            { creatures.length === 0 ? renderNoCreatures() : renderList(creatures) }
        </section>
    );
};

export default list;
