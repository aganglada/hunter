export default (props) => {
    return (
        <section className={`c-panel${props.open ? ' c-panel--open' : ''}`}>
            { props.flipped ? props.flippedTemplate : props.nonFlippedTemplate }
        </section>
    );
}
