import React from 'react';

import { FiltersConstants } from 'views/manager/manager.filters.reducer';
import { CreaturesConstants } from 'views/manager/manager.creatures.reducer';

const filters = (props, { store }) => {
    let input;

    return (
        <nav className="u-text-align--right u-padding">
            Sort by
            <select
                ref={node => {
                    input = node;
                }}
                className="e-select u-margin-left i-svg__arrow-down"
                name="sortBy"
                id="sortBy"
                onChange={() => {
                    store.dispatch({
                        type: FiltersConstants.SET_FILTERS,
                        sortBy: input.value
                    });
                    store.dispatch({
                        type: CreaturesConstants.SET_ORDER,
                        sortBy: input.value
                    })
                }}
            >
                <option value="age">Age</option>
                <option value="name">Name</option>
                <option value="mana">Mana</option>
                <option value="capturedAt">Captured time</option>
            </select>
        </nav>
    );
};

filters.contextTypes = {
    store: React.PropTypes.object
};

export default filters;
