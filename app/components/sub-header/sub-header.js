const subHeader = ({
    onHunt
}) => {
    return (
        <nav className="c-sub-header u-text-align--center">
            <button
                onClick={onHunt}
                className="e-button e-button--blue"
            >
                Hunt for more!
            </button>
        </nav>
    );
};

export default subHeader;
