import React from 'react';

import CreatureWrapper from 'components/creature-wapper/creature-wrapper';

const creature = ({
    creature
}, { store }) => {

    return (
        <CreatureWrapper hidden={creature.out}>
             <p className="u-padding">
                <small className="u-display--block"><strong>Age</strong> {creature.age}</small>
                <small className="u-display--block"><strong>Mana</strong> {creature.mana}</small>
             </p>
             <h3 className="u-margin-bottom--doubled">{creature.name.toUpperCase()}</h3>
             <button onClick={() => {
                store.dispatch({ type: 'TOGGLE_RELEASE_CREATURE', creature });
                setTimeout(() => {
                    store.dispatch({ type: 'TOGGLE_RELEASE_CREATURE', creature });
                }, 15000);
             }} className="e-button u-width--full">Release it</button>
       </CreatureWrapper>
    );
};

creature.contextTypes = {
    store: React.PropTypes.object
};

export default creature;
