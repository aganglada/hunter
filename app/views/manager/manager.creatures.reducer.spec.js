import creaturesReducer from './manager.creatures.reducer';

describe('Creatures reducer', () => {

    describe('ADD_CREATURE', () => {
        const props = [
            'name',
            'age',
            'mana',
            'capturedAt',
            'out'
        ];
        const stateBefore = [];
        const action = {
            type: 'ADD_CREATURE',
            name: 'Medusa'
        };

        const state = creaturesReducer(stateBefore, action);

        it('should add a new creature to our collection', () => {
            expect(state.length).toEqual(1);
        });

        it('should have all the props', () => {
            props.map((prop) => {
                expect(state[0][prop]).toBeDefined();
            })
        });

        it('name should be the same as we added in the action', () => {
            expect(state[0].name).toEqual(action.name);
        });

        it('age should be an integer between 0 and 500', () => {
            expect(state[0].age >= 0 && state[0].age <= 500).toBe(true);
        });

        it('mana should be an integer between 1000 and 10000', () => {
            expect(state[0].mana >= 1000 && state[0].mana <= 10000).toBe(true);
        });

        it('out should be false', () => {
            expect(state[0].out).toBe(false);
        });

        it('capturedAt should be a string', () => {
            expect(typeof state[0].capturedAt).toBe('string');
        });
    });

    describe('TOGGLE_RELEASE_CREATURE', () => {
        const stateBefore = [{
            name: 'Medusa',
            age: 200,
            mana: 7000,
            out: false
        }, {
            name: 'Medusa 2',
            age: 500,
            mana: 5050,
            out: false
        }];

        const action = {
            type: 'TOGGLE_RELEASE_CREATURE',
            creature: stateBefore[1]
        };

        const stateAfter = [{
            name: 'Medusa',
            age: 200,
            mana: 7000,
            out: false
        }, {
            name: 'Medusa 2',
            age: 500,
            mana: 5050,
            out: true
        }];

        it('should modify the out prop from the given object into out collection', () => {
            expect(creaturesReducer(stateBefore, action)).toEqual(stateAfter);
        });
    });

});
