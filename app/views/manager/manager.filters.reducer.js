export const FiltersConstants = {
    SET_FILTERS: 'SET_FILTERS'
};

export default (state = { sortBy: 'age' }, action) => {
    switch (action.type) {
        case FiltersConstants.SET_FILTERS:
            return {
                ...state,
                sortBy: action.sortBy
            };
        default:
            return state;
    }
}
