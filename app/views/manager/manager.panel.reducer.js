export const PanelConstants = {
    TOGGLE: 'TOGGLE',
    FLIP: 'FLIP'
};

export default (state = { open: false, flipped: false }, action) => {
    switch (action.type) {
        case PanelConstants.TOGGLE:
            return {
                ...state,
                open: !state.open
            };
        case PanelConstants.FLIP:
            return {
                ...state,
                flipped: !state.flipped
            };
        default:
            return state;
    }
}
