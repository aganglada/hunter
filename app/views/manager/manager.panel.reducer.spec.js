import panelReducer from './manager.panel.reducer';

describe('Panel Reducer', () => {

    const stateBefore = { open: false, flipped: false };

    describe('TOGGLE', () => {
        const action = {
            type: 'TOGGLE'
        };
        const stateAfter = { open: true, flipped: false };

        it('should toggle the open key of our panel', () => {
            expect(panelReducer(stateBefore, action)).toEqual(stateAfter);
        });
    });

    describe('FLIP', () => {
        const action = {
            type: 'FLIP'
        };
        const stateAfter = { open: false, flipped: true };

        it('should toggle the flipped key of our panel', () => {
            expect(panelReducer(stateBefore, action)).toEqual(stateAfter);
        });
    });
});
