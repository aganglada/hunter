import { Component } from 'react';
import { Provider } from 'react-redux';

import store from './manager.store';
import mapsConfig from 'config/maps.json';

import List from 'components/list/list';
import Panel from 'components/panel/panel';
import SubHeader from 'components/sub-header/sub-header';

import sortByKey from 'services/sort-by-key/sort-by-key';
import sumByKey from 'services/sum-by-key/sum-by-key';
import url from 'services/url/url';
import hunt from 'services/hunt/hunt';

class Manager extends Component {

    componentDidMount() {
        this.unsubscribe = store.subscribe(() => {
            this.forceUpdate();
        });
    }

    componentWillUnMount() {
        this.unsubscribe();
    }

    render() {
        const { creatures, filters, panel } = store.getState();

        const sortedCreatures = sortByKey(creatures, filters.sortBy);
        const totalAge = sumByKey(creatures, 'age');
        const totalMana = sumByKey(creatures, 'mana');
        const mapsUrl = `${mapsConfig.base}?${url.objectToParams(mapsConfig.params)}`;

        return (
            <Provider store={store}>
                <article>
                    <SubHeader onHunt={hunt.start.bind(null, store)}/>
                    <List
                        creatures={sortedCreatures}
                        totalAge={totalAge}
                        totalMana={totalMana}
                    />
                    <Panel open={panel.open} flipped={panel.flipped}
                        nonFlippedTemplate={(<div>
                            <img className="c-panel__map u-width--full" src={mapsUrl} />
                             <h1 className="c-panel__map--text">Looking for creatures...</h1>
                        </div>)}
                        flippedTemplate={(<div className="u-text-align--center">
                            <h1>You found a creature!</h1>
                            <form onSubmit={(event) => {
                                event.preventDefault();
                                store.dispatch({ type: 'ADD_CREATURE', name: this.refs.name.value });
                                store.dispatch({ type: 'FLIP' });
                                store.dispatch({ type: 'TOGGLE' });
                            }}>
                                <button onClick={() => {
                                    store.dispatch({ type: 'FLIP' });
                                    store.dispatch({ type: 'TOGGLE' });
                                }} className="e-button u-margin-left" type="button">I don't like it</button>
                                <input ref="name" placeholder="Give him a name" className="e-input" type="text" id="name" name="name" required />
                                <button className="e-button e-button--blue" type="submit">Add to your collection</button>
                            </form>
                        </div>)}
                    />
                </article>
            </Provider>
        );
    }
}

export default Manager;
