import random from 'services/random/random';
import date from 'services/date/date';

export const CreaturesConstants = {
    SET_ORDER: 'SET_ORDER',
    ADD_CREATURE: 'ADD_CREATURE',
    TOGGLE_RELEASE_CREATURE: 'TOGGLE_RELEASE_CREATURE'
};

export default (state = [], action) => {
    switch (action.type) {
        case CreaturesConstants.ADD_CREATURE:
            return [
                ...state,
                {
                    name: action.name,
                    age: random(0, 500),
                    mana: random(1000, 10000),
                    capturedAt: date.getCurrentFormattedDate(),
                    out: false
                }
            ];
        case CreaturesConstants.TOGGLE_RELEASE_CREATURE:
            const index = state.indexOf(action.creature);
            state[index].out = !state[index].out;
            return state;
        default:
            return state;
    }
}
