import filtersReducer from './manager.filters.reducer';

describe('Filters Reducer', () => {

    describe('SET_FILTERS', () => {
        const stateBefore = { sortBy: 'age' };
        const action = {
            type: 'SET_FILTERS',
            sortBy: 'mana'
        };
        const stateAfter = { sortBy: action.sortBy };

        it('should modify the sortBy key of filters', () => {
            expect(filtersReducer(stateBefore, action)).toEqual(stateAfter);
        })
    });
});
