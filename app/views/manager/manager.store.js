import { createStore, combineReducers } from 'redux';

import creatures from './manager.creatures.reducer';
import filters from './manager.filters.reducer';
import panel from './manager.panel.reducer';

const reducer = combineReducers({
   creatures,
   filters,
   panel
});

export default createStore(reducer);
