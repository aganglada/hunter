import React from 'react';
import { render } from 'react-dom';
import Header from 'components/header/header';
import Manager from 'views/manager/manager';

class App extends React.Component {
    render() {
        return (
            <main>
                <Header></Header>
                <Manager></Manager>
            </main>
        );
    }
}

render((
    <App></App>
), document.getElementById('js-app'));
